import { Component, ViewChild, ElementRef, ANALYZE_FOR_ENTRY_COMPONENTS } from "@angular/core";
import * as moment from 'moment';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ScriptService } from '../service/script-service';

@Component({
  selector: "my-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  name = "Angular";
  @ViewChild("videoPlayer", { static: false }) videoplayer: ElementRef;
  isPlay: boolean = false;
  videoState: VideoState = { duration: "", currentTime: "", name: "Please Load Video" }
  bookmarks: any = [];// [{ id: 1, duration: 300, percentage: 50 }, { id: 2, duration: 300, percentage: 20 }, { id: 3, duration: 300, percentage: 80 }];
  title = 'appBootstrap';

  closeResult: string;

  constructor(private scriptService: ScriptService, private modalService: NgbModal) {

    this.scriptService.load('youtubeHTML');
  }


  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  loadBookMarks = (key) => {
    this.bookmarks = JSON.parse(sessionStorage.getItem(key))
  }

  saveBookMarks = (key) => {
    sessionStorage.setItem(key, JSON.stringify(this.bookmarks));
  }

  addBookMark = () => {
    var video: any = document.getElementById("my_video_1");
    // alert((100 / video.duration) * video.currentTime)

    if (this.videoState && this.videoState.name && this.videoState.name != "" && this.videoState.name != "Please Load Video") {
      let bm: any = { id: (this.bookmarks ? this.bookmarks.length : 0) + 1, duration: video.currentTime, percentage: (100 / video.duration) * video.currentTime, show: false };
      if (this.bookmarks)
        this.bookmarks.push(bm)
      else
        this.bookmarks = [bm]

      this.saveBookMarks(this.videoState.name)

    }
  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
  playPause() {
    var myVideo: any = document.getElementById("my_video_1");
    if (myVideo.paused) myVideo.play();
    else myVideo.pause();
  }

  clearVideo() {
    var myVideo: any = document.getElementById("my_video_1");
    myVideo.src = "";
    this.videoState = { duration: "", currentTime: "", name: "" }
    // myVideo.abort();
  }
  showBmDetails(bm, stat) {
    //  alert(bm)
    //   console.log()
    bm.show = stat;
  }
  convertSecondsToTime = (SECONDS, format) => {
    if (format == "hh")
      return new Date(SECONDS * 1000).toISOString().substr(11, 2)

    if (format == "mm")
      return new Date(SECONDS * 1000).toISOString().substr(14, 2)

    if (format == "ss")
      return new Date(SECONDS * 1000).toISOString().substr(17, 2)
  }

  pasteContent = (id) => {

    navigator.clipboard.readText()
      .then(text => {
        console.log('Pasted content: ', text);
        let dest: any = document.getElementById(id);
        dest.value = text;
      })
      .catch(err => {
        console.error('Failed to read clipboard contents: ', err);
      });

  }

  playBookMark(bm) {
    let video: any = document.getElementById("my_video_1");

    video.currentTime = bm.duration;
  }

  makeBig() {
    var myVideo: any = document.getElementById("my_video_1");
    myVideo.width = 560;
  }

  makeSmall() {
    var myVideo: any = document.getElementById("my_video_1");
    myVideo.width = 320;
  }

  makeNormal() {
    var myVideo: any = document.getElementById("my_video_1");
    myVideo.width = 420;
  }

  skip(value) {
    let video: any = document.getElementById("my_video_1");
    video.currentTime += value;
  }

  restart() {
    let video: any = document.getElementById("my_video_1");
    video.currentTime = 0;
  }

  jumpto = () => {
    let video: any = document.getElementById("my_video_1");

    let hour: any = document.getElementById("my_hour_1");
    let min: any = document.getElementById("my_min_1");
    let sec: any = document.getElementById("my_sec_1");
    video.currentTime = (!isNaN(parseInt(hour.value)) ? parseInt(hour.value) * 60 * 60 : 0) + (!isNaN(parseInt(min.value)) ? parseInt(min.value) * 60 : 0) + (!isNaN(parseInt(sec.value)) ? parseInt(sec.value) : 0)

    //alert(video.currentTime)
  }


  selectLocalFile = (event: any) => {
    debugger;
    let video: any = document.getElementById("my_video_1");
    var file = event.target.files[0];
    var type = file.type;
    var videoNode = document.querySelector('video');
    var canPlay = videoNode.canPlayType(type);

    var message = 'Can play type "' + type + '": ' + canPlay
    if (canPlay === '')
      alert("cannot play this file format")
    //  displayMessage(message, isError)

    var fileURL = URL.createObjectURL(file)
    video.src = fileURL
    video.play();
    this.videoState.name = file.name;
    console.log(video)

    this.modalService.dismissAll();
    this.loadBookMarks(file.name);
  }


  openScreenCastVideo = () => {
    debugger;
    let video: any = document.getElementById("my_video_1");
    let embed: any = document.getElementById("screencastEmbed");
    let file: any = document.getElementById("screencastFileName");

    var wrapper = document.createElement('div');
    wrapper.innerHTML = embed.value;
    let iframe: any = wrapper.getElementsByTagName("iframe")[0];
    //let iframe: any = wrapper.firstChild;

    debugger;
    let filename: any = file.value;

    if (filename.indexOf(".mp4") == -1)
      filename += ".mp4"

    let finalLink: any = iframe.src.replace("www.", "content.").replace("embed", filename)
    //alert(finalLink);
    //finalLink = 'https://www.youtube.com/watch?v=NifSg0zl8u0';
    video.src = finalLink;
    video.play();



    this.videoState.name = file.value;
    this.modalService.dismissAll();
    this.loadBookMarks(file.value);
  }

  onVideoMetaDataChange = () => {
    let video: any = document.getElementById("my_video_1");
    this.videoState.duration = moment.utc(video.duration * 1000).format('HH:mm:ss');
  }

  onVideoTimeChange = () => {
    //debugger;
    let video: any = document.getElementById("my_video_1");
    let seekBar: any = document.getElementById("my_seekBar_1");
    this.videoState.currentTime = moment.utc(video.currentTime * 1000).format('HH:mm:ss')
    // console.log(moment.utc(video.duration * 1000).format('HH:mm:ss'))
    //  console.log(video.duration)
    // console.log(video.currentTime)
    var value = (100 / video.duration) * video.currentTime;

    // Update the slider value
    seekBar.value = value;
  }

  formatDuration(duration) {
    return moment.utc(duration * 1000).format('HH:mm:ss')
  }


  onSeekbardrag = () => {

    let video: any = document.getElementById("my_video_1");
    let seekBar: any = document.getElementById("my_seekBar_1");
    var time = video.duration * (seekBar.value / 100);

    // Update the video time
    video.currentTime = time;
  }
}


class VideoState {
  currentTime: String = ""
  duration: String = ""
  name: String = ""
}
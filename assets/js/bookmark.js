/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    $('ul.playNav li').on('click', function () {
        $(this).parent().find('a.active').removeClass('active');
        $(this).find('a').addClass('active');
    });
    $('.selectedLine').click(function () {
        // $('#mainRangeSlider .bm').addClass('hide');
        // $(this).parent('.bm').removeClass('hide');
    });
//    $('.selectedLine').click(function () {
//        $('#mainRangeSlider .bm').addClass('hideHalf');
//        $(this).parent('.bm').removeClass('hideHalf');
//    });
//    if ($('.bm').hasClass('.hideHalf')) {
//        $(this).find('.selectedLine').removeClass('active');
//    }
});

function loginActive() {
    $('#blankArea').removeClass('hide');
    $('#blankArea .loginArea').removeClass('hide');
    $('#mainContent').addClass('hide');
    $('#loginActive').addClass('active');
    $('#loginClose').click(function () {
        $('#blankArea .loginArea').addClass('hide');
        //                    $('#loginActive').addClass('active');
    });
    $('#btnLogin').click(function () {
        $('#afterLogin').removeClass('hide');
        $('#blankArea .loginArea').addClass('hide');
    });
    $('#doneBtn').click(function () {
        $('#blankArea .loginArea').addClass('hide');
        $('#mainContent').removeClass('hide');
        $('#loginActive').removeClass('active');
        $('#afterLogin').addClass('hide');
    });
}

function addBookmarkOption() {
    $('#videoGadgetFullView').hide();
    $('#addNewGadget').show();
    $('#closeAddbookmarkOption').click(function () {
        $('#videoGadgetFullView').show();
        $('#addNewGadget').hide();
    });
}

function addBookmarkOption1() {
    $('#videoGadgetFullView1').hide();
    $('#addNewGadget1').show();
    $('#closeAddbookmarkOption1').click(function () {
        $('#videoGadgetFullView1').show();
        $('#addNewGadget1').hide();
    });
}

function openRecommendedPopup() {
    $('#recommandedBookmarks').removeClass('hide');
    $('#openRecommendedPopupBtn').addClass('active');
    $('#mainContentBar').removeClass('alt');
    if ($("#leftSideBar").hasClass('hideLeftSideBar')) {
        $('#leftSideBar').removeClass('hideLeftSideBar');
        $('#mainContentBar').removeClass('alt');
        $('#hideLeftSideBarLink').toggleClass('alt');
        $('#hideLeftSideBarLink span').toggleClass('hide');
    }
    $('#closeRecommandedBookmarks').click(function () {
        $('#recommandedBookmarks').addClass('hide');
        $('#openRecommendedPopupBtn').removeClass('active');
    });
}

function openSelectBookMarkModal() {
    $('#selectBookMarkModal').removeClass('hide');
//    $('#openRecommendedPopupBtn').addClass('active');
    $('#mainContentBar').removeClass('alt');
    if ($("#leftSideBar").hasClass('hideLeftSideBar')) {
        $('#leftSideBar').removeClass('hideLeftSideBar');
        $('#mainContentBar').removeClass('alt');
        $('#hideLeftSideBarLink').toggleClass('alt');
        $('#hideLeftSideBarLink span').toggleClass('hide');
    }
    $('#closeSelectBookMarkModal').click(function () {
        $('#selectBookMarkModal').addClass('hide');
//        $('#openRecommendedPopupBtn').removeClass('active');
    });
}

function openMyClipsModal() {
    $('#myClipsModal').removeClass('hide');
    $('#mainContentBar').removeClass('alt');
    if ($("#leftSideBar").hasClass('hideLeftSideBar')) {
        $('#leftSideBar').removeClass('hideLeftSideBar');
        $('#mainContentBar').removeClass('alt');
        $('#hideLeftSideBarLink').toggleClass('alt');
        $('#hideLeftSideBarLink span').toggleClass('hide');
    }
    $('#closeMyClipsModal').click(function () {
        $('#myClipsModal').addClass('hide');
    });
}

function normalPlay() {
    $('ul.bookmarkVideoPlayOptions').css('display', 'inline-block');
    hideVideoPlaylist();
    if ($('#myClipsModal').hasClass('hide')) {
    } else {
        $('#myClipsModal').addClass('hide');
    }
    $('#youTubeTimeLink').removeClass('hide');
    $('#youTubeTimeLink1').removeClass('hide');
    if ($("#leftSideBar").hasClass('hideLeftSideBar')) {
//        $('#leftSideBar').removeClass('hideLeftSideBar');
//        $('#mainContentBar').removeClass('alt');
//        $('#hideLeftSideBarLink').toggleClass('alt');
//        $('#hideLeftSideBarLink span').toggleClass('hide');
    } else {
    }
    $('#hideBtn').removeClass('hide');
    $('#youTubeTimeline').removeClass('hide');
    $("#bookmarkArrows").removeClass('hide');
    $("#bookmarkText").removeClass('hide');
    $('#leftSideBar').removeClass('activeNotes');
    $('#mainContentBar').removeClass('activeNotes');
    $('#mainContent').show();
    $('#videoDetails').show();
    $('#notesPlay').hide();
    $('#smallVideoPlay').hide();
    $('#keyLearningPopup').hide();
    $('ul.playNav li').find('a.active').removeClass('active');
    $('#normalPlayBtn2').addClass('active');
    $('#normalPlayBookmarkOptions').removeClass('hide');
    $('#keyLearningBookmarkOptions').addClass('hide');
    $('#sideBarBookmarks').addClass('hide');
    $('#sideBarLinks').removeClass('hide');
    $('#hideLeftSideBarLink').show();
    $('#mainContentBar').removeClass('activeKey');
}

function notesPlay() {
    normalPlay();
    $('#youTubeTimeLink').addClass('hide');
    $('#youTubeTimeLink1').addClass('hide');
    $('#leftSideBar').addClass('activeNotes');
    $('#mainContentBar').addClass('activeNotes');
    $('#mainContent').hide();
    $('#videoDetails').hide();
    $('#notesPlay').show();
    $('#smallVideoPlay').show();
    $('#keyLearningPopup').hide();
    $('ul.playNav li').find('a.active').removeClass('active');
    $('#notesPlayBtn3').addClass('active');
    $('#sideBarBookmarks').removeClass('hide');
    $('#recommandedBookmarks').addClass('hide');
    $('#selectBookMarkModal').addClass('hide');
    $('#sideBarLinks').addClass('hide');
    $('#hideLeftSideBarLink').hide();
}

function openKeyLearningPoup() {
    normalPlay();
    $('ul.bookmarkVideoPlayOptions').css('display', 'none');
    if ($("#leftSideBar").hasClass('hideLeftSideBar')) {
        $('#leftSideBar').removeClass('hideLeftSideBar');
        $('#mainContentBar').removeClass('alt');
        $('#hideLeftSideBarLink').toggleClass('alt');
        $('#hideLeftSideBarLink span').toggleClass('hide');
    } else {

    }

//    $('#mainContentBar').addClass('activeKey');
    $('#normalPlayBookmarkOptions').addClass('hide');
    $('#keyLearningBookmarkOptions').removeClass('hide');
    $('ul.playNav li').find('a.active').removeClass('active');
    $('#keyLearningBtn2').addClass('active');
    $('#keyLearningPopup').show();
    $('.keyLearningClose').click(function () {
        $('#normalPlayBookmarkOptions').removeClass('hide');
        $('#keyLearningBookmarkOptions').addClass('hide');
        $('#keyLearningPopup').hide();
        $('ul.playNav li').find('a.active').removeClass('active');
        $('#normalPlayBtn2').addClass('active');
        $('#mainContentBar').removeClass('activeKey');
    });
}

function openKeyLearningBookmarkPopup() {
    $('.keyLearningBookmarkPopup').show();
//    $('#playNavOptions').hide();
//    $('#selectedLine2').addClass("active");
//    $('#selectedLineDetails2').addClass("active");
//    $('#closeBm2').click(function () {
//        $('#selectedLine2').removeClass("active");
//        $('#selectedLineDetails2').removeClass("active");
//        $('#playNavOptions').show();
//    });
}

function openBookmarkOptions() {
//    $('.keyLearningBookmarkPopup').show();
    $('#playNavOptions').hide();
    $('#selectedLine2').addClass("active");
    $('#selectedLineDetails2').addClass("active");
    $('#closeBm2').click(function () {
        $('#selectedLine2').removeClass("active");
        $('#selectedLineDetails2').removeClass("active");
        $('#playNavOptions').show();
    });
}

function openBookmarkOptions1() {
//    $('.keyLearningBookmarkPopup').show();
    $('#playNavOptions').hide();
    $('#selectedLine4').addClass("active");
    $('#selectedLineDetails4').addClass("active");
    $('#bm4').show();
    $('#closeBm4').click(function () {
        $('#selectedLine4').removeClass("active");
        $('#selectedLineDetails4').removeClass("active");
        $('#playNavOptions').show();
        $('#bm4').hide();
    });
}

function closeKeyLearningBookmarkPopup() {
    $('.keyLearningBookmarkPopup').hide();
    $('#playNavOptions').show();
    $('#selectedLine2').removeClass("active");
    $('#selectedLineDetails2').removeClass("active");
    $('#closeBm2').click(function () {
        $('#selectedLine2').addClass("active");
        $('#selectedLineDetails2').addClass("active");
        $('#playNavOptions').hide();
    });
}

function createNewBookmark() {
    $('#mainRangeSlider .bm').find('.selectedLine.active').removeClass("active");
    $('#mainRangeSlider .bm').find('.selectedLineDetails.active').removeClass("active");
    // $('#mainRangeSlider .bm').addClass('hide');
    $('#bmDuration').hide();
    $('#newBookmark').removeClass('hide');
    $('#playNavOptions').hide();
    $('#newSelectedLine').addClass("active");
    $('#newSelectedLineDetails').addClass("active");
    $('#closeNewBookmark').click(function () {
        $('#newSelectedLine').removeClass("active");
        $('#newSelectedLineDetails').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
    $('#closeNewBookmarkCopy').click(function () {
        $('#newSelectedLine').removeClass("active");
        $('#newSelectedLineDetails').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
}

$('#videoGadgetGrids .changeView a').click(function () {
    $('#videoGadgetGrids').toggleClass('scrollable');
    $('#videoGadgetGrids .changeView a').toggleClass('hide');
});

$('#videoGadgetGrids1 .changeView a').click(function () {
    $('#videoGadgetGrids1').toggleClass('scrollable');
    $('#videoGadgetGrids1 .changeView a').toggleClass('hide');
});

$('#hideLeftSideBarLink').click(function () {
    $('#leftSideBar').toggleClass('hideLeftSideBar');
    $('#hideLeftSideBarLink').toggleClass('alt');
    $('#hideLeftSideBarLink span').toggleClass('hide');
    $('#mainContentBar').toggleClass('alt');
    if ($('#selectBookMarkModal').hasClass('hide')) {
    } else {
        $('#selectBookMarkModal').addClass('hide');
    }
    if ($('#myClipsModal').hasClass('hide')) {
    } else {
        $('#myClipsModal').addClass('hide');
    }
    if ($('#recommandedBookmarks').hasClass('hide')) {
    } else {
        $('#recommandedBookmarks').addClass('hide');
    }
});

$('#selectedLine1').click(function () {
    $('#playNavOptions').hide();
    $('#selectedLine1').addClass("active");
    $('#selectedLineDetails1').addClass("active");
    $('#bmDuration').show();
    $('#closeBm1').click(function () {
        $('#selectedLine1').removeClass("active");
        $('#selectedLineDetails1').removeClass("active");
        $('#bmDuration').hide();
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine2').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine2').addClass("active");
    $('#selectedLineDetails2').addClass("active");
    $('#closeBm2').click(function () {
        $('#selectedLine2').removeClass("active");
        $('#selectedLineDetails2').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine3').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine3').addClass("active");
    $('#selectedLineDetails3').addClass("active");
    $('#closeBm3').click(function () {
        $('#selectedLine3').removeClass("active");
        $('#selectedLineDetails3').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine4').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine4').addClass("active");
    $('#selectedLineDetails4').addClass("active");
    $('#closeBm4').click(function () {
        $('#selectedLine4').removeClass("active");
        $('#selectedLineDetails4').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine5').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine5').addClass("active");
    $('#selectedLineDetails5').addClass("active");
    $('#closeBm5').click(function () {
        $('#selectedLine5').removeClass("active");
        $('#selectedLineDetails5').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine6').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine6').addClass("active");
    $('#selectedLineDetails6').addClass("active");
    $('#closeBm5').click(function () {
        $('#selectedLine5').removeClass("active");
        $('#selectedLineDetails5').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});
$('#selectedLine7').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine7').addClass("active");
    $('#selectedLineDetails7').addClass("active");
    $('#closeBm5').click(function () {
        $('#selectedLine5').removeClass("active");
        $('#selectedLineDetails5').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});
$('#selectedLine8').click(function () {
    $('.selectedLine').removeClass("active");
    $('.selectedLineDetails').removeClass("active");
    $('#playNavOptions').hide();
    $('#selectedLine8').addClass("active");
    $('#selectedLineDetails8').addClass("active");
    $('#closeBm5').click(function () {
        $('#selectedLine5').removeClass("active");
        $('#selectedLineDetails5').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#newBookmark #square').click(function () {
    $('#mainRangeSlider .bm').find('.selectedLine.active').removeClass("active");
    $('#mainRangeSlider .bm').find('.selectedLineDetails.active').removeClass("active");
    $('#mainRangeSlider .bm').addClass('hide');
    $('#newBookmark').removeClass('hide');
    $('#playNavOptions').hide();
    $('#newSelectedLine').addClass("active");
    $('#newSelectedLineDetails').addClass("active");
    $('#closeNewBookmark').click(function () {
        $('#newSelectedLine').removeClass("active");
        $('#newSelectedLineDetails').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
    $('#closeNewBookmarkCopy').click(function () {
        $('#newSelectedLine').removeClass("active");
        $('#newSelectedLineDetails').removeClass("active");
        $('#playNavOptions').show();
        $('#mainRangeSlider .bm').removeClass('hide');
    });
});

$('#selectedLine11').click(function () {
    $('#playNavOptions').hide();
    $('#selectedLine11').addClass("active");
    $('#selectedLineDetails11').addClass("active");
    $('#closeBm11').click(function () {
        $('#playNavOptions').show();
        $('#selectedLine11').removeClass("active");
        $('#selectedLineDetails11').removeClass("active");
    });
});

$('#selectedLine12').click(function () {
    $('#playNavOptions').hide();
    $('#selectedLine12').addClass("active");
    $('#selectedLineDetails12').addClass("active");
    $('#closeBm12').click(function () {
        $('#playNavOptions').show();
        $('#selectedLine12').removeClass("active");
        $('#selectedLineDetails12').removeClass("active");
    });
});

$('#selectedLine13').click(function () {
    $('#playNavOptions').hide();
    $('#selectedLine13').addClass("active");
    $('#selectedLineDetails13').addClass("active");
    $('#closeBm13').click(function () {
        $('#playNavOptions').show();
        $('#selectedLine13').removeClass("active");
        $('#selectedLineDetails13').removeClass("active");
    });
});

$('#selectedLine11').click(function () {
    $('#playNavOptions').hide();
    $('#selectedLine11').addClass("active");
    $('#selectedLineDetails11').addClass("active");
    $('#closeBm11').click(function () {
        $('#playNavOptions').show();
        $('#selectedLine11').removeClass("active");
        $('#selectedLineDetails11').removeClass("active");
    });
});

$('#bookmarkClipOptionBtn1').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptions1').removeClass('hide');
    $('#bookmarkClipOptionBtn1').addClass('hide');
    $('#bookmarkClipPlayBtn1').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp2').hide();
    $('#bcp3').hide();
    $('#bcp4').hide();
    $('#bcp5').hide();
    $('#bcpClose1').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptions1').addClass('hide');
        $('#bookmarkClipOptionBtn1').removeClass('hide');
        $('#bookmarkClipPlayBtn1').removeClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp2').show();
        $('#bcp3').show();
        $('#bcp4').show();
        $('#bcp5').show();
    });
});

$('#bookmarkClipOptionBtn2').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptions2').removeClass('hide');
    $('#bookmarkClipOptionBtn2').addClass('hide');
    $('#bookmarkClipPlayBtn2').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp3').hide();
    $('#bcp4').hide();
    $('#bcp5').hide();
    $('#bcpClose2').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptions2').addClass('hide');
        $('#bookmarkClipOptionBtn2').removeClass('hide');
        $('#bookmarkClipPlayBtn2').removeClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp1').show();
        $('#bcp3').show();
        $('#bcp4').show();
        $('#bcp5').show();
    });
});

$('#bookmarkClipOptionBtn3').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptions3').removeClass('hide');
    $('#bookmarkClipOptionBtn3').addClass('hide');
    $('#bookmarkClipPlayBtn3').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp2').hide();
    $('#bcp4').hide();
    $('#bcp5').hide();
    $('#bcpClose3').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptions3').addClass('hide');
        $('#bookmarkClipOptionBtn3').removeClass('hide');
        $('#bookmarkClipPlayBtn3').removeClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp1').show();
        $('#bcp2').show();
        $('#bcp4').show();
        $('#bcp5').show();
    });
});

$('#bookmarkClipOptionBtn4').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptions4').removeClass('hide');
    $('#bookmarkClipOptionBtn4').addClass('hide');
    $('#bookmarkClipPlayBtn4').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp2').hide();
    $('#bcp3').hide();
    $('#bcp5').hide();
    $('#bcpClose4').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptions4').addClass('hide');
        $('#bookmarkClipOptionBtn4').removeClass('hide');
        $('#bookmarkClipPlayBtn4').removeClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp1').show();
        $('#bcp2').show();
        $('#bcp3').show();
        $('#bcp5').show();
    });
});

$('#bookmarkClipOptionBtn5').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptions5').removeClass('hide');
    $('#bookmarkClipOptionBtn5').addClass('hide');
    $('#bookmarkClipPlayBtn5').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp2').hide();
    $('#bcp3').hide();
    $('#bcp4').hide();
    $('#bcpClose5').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptions5').addClass('hide');
        $('#bookmarkClipOptionBtn5').removeClass('hide');
        $('#bookmarkClipPlayBtn5').addClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp1').show();
        $('#bcp2').show();
        $('#bcp3').show();
        $('#bcp4').show();
    });
});

$('#bookmarkClipOptionBtnNew').click(function () {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptionsNew').removeClass('hide');
    $('#bookmarkClipOptionBtnNew').addClass('hide');
    $('#bookmarkClipPlayBtnNew').addClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp2').hide();
    $('#bcp3').hide();
    $('#bcp4').hide();
    $('#bcp5').hide();
    $('#bcpCloseNew').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptionsNew').addClass('hide');
        $('#bookmarkClipOptionBtnNew').removeClass('hide');
        $('#bookmarkClipPlayBtnNew').addClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#bcp1').show();
        $('#bcp2').show();
        $('#bcp3').show();
        $('#bcp4').show();
        $('#bcp5').show();
    });
});

function createNewClip() {
    $('#editBookmark').removeClass('hide');
    $('#currentVideo').removeClass('hide');
    $('#bookmarkClipOptionsNew').removeClass('hide');
//    $('#bookmarkClipOptionBtnNew').addClass('hide');
    $('#bookmarkClipPlayBtnNew').addClass('hide');
    $('#newClip .bookmarkLine').removeClass('hide');
    $('#newClip i').removeClass('hide');
    $('#playNavOptions').hide();
    $('#bcp1').hide();
    $('#bcp2').hide();
    $('#bcp3').hide();
    $('#bcp4').hide();
    $('#bcp5').hide();
    $('#bcpCloseNew').click(function () {
        $('#playNavOptions').show();
        $('#bookmarkClipOptionsNew').addClass('hide');
//        $('#bookmarkClipOptionBtnNew').removeClass('hide');
        $('#bookmarkClipPlayBtnNew').addClass('hide');
        $('#editBookmark').addClass('hide');
        $('#currentVideo').addClass('hide');
        $('#newClip .bookmarkLine').addClass('hide');
        $('#newClip i').addClass('hide');
        $('#bcp1').show();
        $('#bcp2').show();
        $('#bcp3').show();
        $('#bcp4').show();
        $('#bcp5').show();
    });
}

$('#playNavOptions ul li a').click(function () {
    $('#mainRangeSlider .selectedLineDetails').removeClass('active');
    $('#mainRangeSlider .selectedLine').removeClass('active');
});